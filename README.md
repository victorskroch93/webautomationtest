# WebAutomationTest



## Planning

Hello, attached is an Excel spreadsheet with Test Planning.

## Execution

Check the attached spreadsheet.

## Automation

Automation testing was performed in Java with IntelliJ

IntelliJ files are attached to the Project in a zip file

Objectives: Automate one of the scenarios on the same news website, as follows:

Scenario: Search a post in the news website
	When I access the news website
	And I search for a post using a existent title
	Then the news website must return the searched post in a results page


